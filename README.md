# Patterns de construction
* Abstract factory
* Builder
* Factory method
* Prototype
* Singleton
* Pluggable Factory (mix Abstract factory + Prototype)


# Patterns de structuration
* Adapter
* Bridge
* Composite
* Decorator
* Facade
* Flyweight
* Proxy

# Patterns de comportement
* Chain of responsibility
* Command
* Interpreter
* Iterator
* Mediator
* Memento
* Observer
* State
* Strategy
* Template method
* Visitor

# Autres (ou à ranger)
* Multicast


# Mémo
Préférer les interfaces aux héritages